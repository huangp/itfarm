package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.User;
import com.tc.itfarm.model.UserCriteria;

public interface UserDao extends SingleTableDao<User, UserCriteria> {
}