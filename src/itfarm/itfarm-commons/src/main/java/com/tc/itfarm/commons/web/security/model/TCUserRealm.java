package com.tc.itfarm.commons.web.security.model;

import com.google.common.collect.Sets;
import com.tc.itfarm.api.exception.LoginFailureException;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.EncoderUtil;
import com.tc.itfarm.model.Log;
import com.tc.itfarm.model.Role;
import com.tc.itfarm.model.RolePrivilege;
import com.tc.itfarm.model.User;
import com.tc.itfarm.service.LogService;
import com.tc.itfarm.service.RolePrivilegeService;
import com.tc.itfarm.service.RoleService;
import com.tc.itfarm.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2016/8/13.
 */
public class TCUserRealm extends AuthorizingRealm {

    private static final Logger logger = LoggerFactory.getLogger(TCUserRealm.class);

    private UserService userService;
    private RolePrivilegeService rolePrivilegeService;
    private RoleService roleService;
    private LogService logService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setRolePrivilegeService(RolePrivilegeService rolePrivilegeService) {
        this.rolePrivilegeService = rolePrivilegeService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public void setLogService(LogService logService) {
        this.logService = logService;
    }

    /**
     * 授权操作
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        User user = (User) principalCollection.getPrimaryPrincipal();
        if (user == null ) {
            logger.error("用户不存在");
            throw new LoginFailureException("用户不存在!");
        }
        Set<String> roleSet = Sets.newHashSet();
        List<Role> roles = roleService.selectByUserId(user.getRecordId());
        for (Role role : roles) {
            roleSet.add(role.getName());
        }
        List<RolePrivilege> rolePrivilegeList = rolePrivilegeService.selectByUserId(user.getRecordId());
        Set<String> privilegeSet = Sets.newHashSet();
        for (RolePrivilege rp : rolePrivilegeList) {
            privilegeSet.add(rp.getPrivilegeCode());
        }
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.addRoles(roleSet);
        authorizationInfo.addStringPermissions(privilegeSet);
        return authorizationInfo;
    }

    /**
     * 身份验证操作
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String username = (String) authenticationToken.getPrincipal();
        User user = userService.selectByUsername(username);
        if (user == null) {
            logger.error("用户不存在");
            throw new AuthenticationException("用户不存在!");
        }
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(user, user.getPassword(), getClass().getName());
        saveLoginLog(user);
        return authenticationInfo;
    }

    private void saveLoginLog(User user) {
        // 记录登陆日志
        Log log = new Log();
        log.setType("系统登陆");
        log.setModifyTime(DateUtils.now());
        log.setModifyTime(DateUtils.now());
        log.setUsername(user.getNickname());
        log.setUserId(user.getRecordId());
        String host = SecurityUtils.getSubject().getSession().getHost();
        log.setNewContent("ip:" + host);
        logService.insert(log);
    }
}
