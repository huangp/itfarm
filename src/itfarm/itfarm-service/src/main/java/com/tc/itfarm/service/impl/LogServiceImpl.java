package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.api.util.PageQueryHelper;
import com.tc.itfarm.dao.LogDao;
import com.tc.itfarm.model.Log;
import com.tc.itfarm.model.LogCriteria;
import com.tc.itfarm.service.LogService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by wangdongdong on 2016/8/17.
 */
@Service
public class LogServiceImpl extends BaseServiceImpl<Log> implements LogService {

    @Resource
    private LogDao logDao;

    @Override
    protected SingleTableDao getSingleDao() {
        return logDao;
    }

    @Override
    public List<Log> selectByType(String type) {
        LogCriteria criteria = new LogCriteria();
        criteria.setOrderByClause(" create_time desc");
        criteria.or().andTypeLike("%" + type + "%");
        return logDao.selectByCriteria(criteria);
    }

    @Override
    public PageList<Log> selectByPage(String type, Page page, Date beginDate, Date endDate) {
        LogCriteria criteria = new LogCriteria();
        LogCriteria.Criteria inner = criteria.createCriteria();
        if (StringUtils.isNotBlank(type)) {
            inner.andTypeLike("%" + type + "%");
        }
        if (beginDate != null) {
            inner.andCreateTimeGreaterThanOrEqualTo(beginDate);
        }
        if (endDate != null) {
            inner.andCreateTimeLessThanOrEqualTo(endDate);
        }
        return PageQueryHelper.queryPage(page, criteria, logDao, " create_time desc");
    }
}
