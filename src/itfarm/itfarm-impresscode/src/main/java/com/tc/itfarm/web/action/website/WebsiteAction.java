package com.tc.itfarm.web.action.website;

import com.tc.itfarm.web.action.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/website")
public class WebsiteAction extends BaseAction{

	private static final Logger LOGGER = LoggerFactory.getLogger(WebsiteAction.class);

	/**
	 * 文章首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request, @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo) {
		ModelAndView mv = new ModelAndView("website/index");
		this.addAttribute(mv);
		mv.addObject("menuSelected", 4);
		return mv;
	}

}
