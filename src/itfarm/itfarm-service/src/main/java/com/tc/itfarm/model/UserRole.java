package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class UserRole implements Serializable {
    private Integer recordId;

    private Integer userId;

    private Integer roleId;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public UserRole(Integer recordId, Integer userId, Integer roleId, Date createTime) {
        this.recordId = recordId;
        this.userId = userId;
        this.roleId = roleId;
        this.createTime = createTime;
    }

    public UserRole() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}