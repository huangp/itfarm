package com.tc.itfarm.web.action.article;

import com.google.common.collect.Lists;
import com.tc.itfarm.api.common.Codes;
import com.tc.itfarm.api.enums.QueryArticleEnum;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.Menu;
import com.tc.itfarm.model.User;
import com.tc.itfarm.service.*;
import com.tc.itfarm.web.biz.ArticleBiz;
import com.tc.itfarm.web.biz.LoginBiz;
import com.tc.itfarm.web.vo.MenuVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.crypto.spec.OAEPParameterSpec;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/article")
public class ArticleAction {

	private static final Logger LOGGER = LoggerFactory.getLogger(ArticleAction.class);
	@Resource
	private ArticleService articleService;
	@Resource
	private ArticleBiz articleBiz;
	@Resource
	private SystemConfigService systemConfigService;
	@Resource
	private MenuService menuService;
	@Resource
	private FavoriteService favoriteService;
	@Resource
	private LoginBiz loginBiz;

	/**
	 * 文章列表
	 * @param pageNo
	 * @param request
	 * @return
	 */
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam(value = "menuId", defaultValue = "") Integer menuId,
							 @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo, HttpServletRequest request) {
		if (menuId == null) {
			return new ModelAndView("redirect:index.do");
		}
		pageNo = pageNo==null ? 0 : pageNo;
		ModelAndView mv = new ModelAndView("article/menu_index");
		Page page = new Page(pageNo, Codes.COMMON_PAGE_SIZE);
		MenuVO vo = articleBiz.getMenuVO(menuId, page);
		mv.addObject("vo", vo);
		mv.addObject("page", page);
		this.addAttribute(mv);
		mv.addObject("menuSelected", menuId);
		return mv;
	}

	/**
	 * 文章首页
	 * @param request
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request, @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo) {
		ModelAndView mv = new ModelAndView("article/index");
		// 系统配置放入session
		request.getSession().setAttribute("config", systemConfigService.selectAll().get(0));
		// 获取菜单
		List<MenuVO> menuVOList = articleBiz.getMenuVOs();
		mv.addObject("list", menuVOList);
		Page page = new Page(pageNo, Codes.COMMON_PAGE_SIZE);
		PageList<Article> articlePageList = articleService.selectArticleByPage(page, null, null);
		mv.addObject("articles", articleBiz.getArticleVOs(articlePageList.getData()));
		mv.addObject("page", articlePageList.getPage());
//		mv.addObject("random", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.RANDOM, 2)));
		this.addAttribute(mv);
		// 菜单默认选择首页
		mv.addObject("menuSelected", 0);
		return mv;
	}

	/**
	 * 单个文章详细
	 * @param id
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/detail")
	public ModelAndView detail(@RequestParam(value = "id", required = true) Integer id, HttpServletRequest request,
							   @RequestParam(value = "widescreen", required = false, defaultValue = "") String widerscreen) throws Exception {
		ModelAndView mv = new ModelAndView("article/detail");
		if (StringUtils.isNotBlank(widerscreen) && (widerscreen.equals("t") || widerscreen.equals("true"))) {
			mv.setViewName("article/detail_full");
		}
		mv.addObject("item", articleBiz.getArticleVOById(id));
		this.addAttribute(mv);
		return mv;
	}

	/**
	 * 初始化数据
	 * @param mv
	 */
	private void addAttribute(ModelAndView mv) {
		// 用于图片滑动展示
		mv.addObject("animations", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.PAGE_VIEW, 5)));
		mv.addObject("newArticles", articleBiz.getArticleVOs(articleService.selectArticles(QueryArticleEnum.CREATE_TIME, 5)));
		mv.addObject("menus", menuService.selectAll());
		User user = loginBiz.getCurUser();
		// 查询用户收藏的文章id集合
		if (user != null) {
			mv.addObject("favoriteIds", favoriteService.selectArticleIds(user.getRecordId()));
		}
	}

}
